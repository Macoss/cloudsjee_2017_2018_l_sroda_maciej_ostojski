package pl.dmcs.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.json.*;

public class Map {

    public static class Maps extends Mapper<LongWritable, Text, Text, DoubleWritable> {

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String line = value.toString();
            String[] tuple = line.split("\\n");
            try {
                for (int i = 0; i < tuple.length; i++) {
                    JSONObject obj = new JSONObject(tuple[i]);
                    JSONObject data = obj.getJSONObject("features2D");

                    String side = obj.getString("side");
                    int sample = obj.getInt("sample");
                    int series = obj.getInt("series");

                    double first = data.getDouble("first");
                    double second = data.getDouble("second");
                    double third = data.getDouble("third");
                    double fourth = data.getDouble("fourth");
                    double fifth = data.getDouble("fifth");

                    context.write(new Text(side + "-" + series + "-" + "first"), new DoubleWritable(first));

                    context.write(new Text(side + "-" + series + "-" + "second"), new DoubleWritable(second));

                    context.write(new Text(side + "-" + series + "-" + "third"), new DoubleWritable(third));

                    context.write(new Text(side + "-" + series + "-" + "fourth"), new DoubleWritable(fourth));

                    context.write(new Text(side + "-" + series + "-" + "fifth"), new DoubleWritable(fifth));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public static class Reduce extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {

        public void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {

            List<Double> doubleValues = new ArrayList<Double>();
            for (DoubleWritable val : values) {
                doubleValues.add(val.get());
            }


            double sum = 0.0;
            for(double a : doubleValues) {
                sum += a;
            }
            double mean = sum/doubleValues.size();

            double temp = 0;
            for(double a :doubleValues)
                temp += (a-mean)*(a-mean);
            double variance = temp/(doubleValues.size()-1);

            double sqrt = Math.sqrt(variance);

            context.write(key, new DoubleWritable(sqrt));
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        if (args.length != 2) {
            System.err.println("Usage: CombineBooks <in> <out>");
            System.exit(2);
        }

        Job job = new Job(conf, "CombineBooks");
        job.setJarByClass(Map.class);
        job.setMapperClass(Maps.class);
        job.setReducerClass(Reduce.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(DoubleWritable.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(DoubleWritable.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
